#include <iostream>
#include <string>
#include <math.h>
using namespace std;

enum typ {kvadraticka, linearni, vsechna, neresitelna};

struct Complex {
	double real;
	double imag;
};

struct Rovnice {
	double a;
	double b;
	double c;
	double x1;
	double x2;
	Complex complex;
	double disc;
	typ type;
};

void clearCin() {
	cin.clear();
	cin.ignore(256, '\n');
}


void mockingZadani(Rovnice* rovnice, int n){

}

void nactiZadani(Rovnice *rovnice, int n) {
	cout << "Zadej " << n << " rovnic ve tvaru ax^2 + bx + c = 0\n";
	for (int i = 0; i < n; i++) {
		cout << "Rovnice c. " << i + 1 << endl;
		string rce[3] = { "a = ","b = ","c = " };
		for (int j = 0; j < 3; j++) {
			while (true) {
				cout << rce[j];
				switch (j) {
				case 0:
					cin >> rovnice[i].a;
					break;
				case 1:
					cin >> rovnice[i].b;
					break;
				case 2:
					cin >> rovnice[i].c;
					break;
				}
				if (!cin) {
					clearCin();
					cout << "Cislo v zadanem tvaru neni platne. Zadejte jej znovu" << endl;
					continue;
				}
				break;
			}
		}
	}
}

void determineType(Rovnice &rovnice) {
	if(rovnice.a == 0) {
		if (rovnice.b == 0) {
			if (rovnice.c == 0) {
				rovnice.type = vsechna;
			}
			else {
				rovnice.type = neresitelna;
			}
		}
		else {
			rovnice.type = linearni;
		}
	}
	else {
	rovnice.type = kvadraticka;
	}
}

void calculate(Rovnice &rovnice) {
	determineType(rovnice);
	rovnice.disc = (rovnice.b * rovnice.b - 4 * rovnice.a * rovnice.c);
	if (rovnice.type == kvadraticka) {
		if (rovnice.disc >= 0) { //dva realne koreny (i dva stejne)
			rovnice.x1 = (-rovnice.b + sqrt(rovnice.disc) / 2 * rovnice.a);
			rovnice.x2 = (-rovnice.b - sqrt(rovnice.disc) / 2 * rovnice.a);
		}
		else { //dva komplexni koreny
			rovnice.complex.real = (-rovnice.b) / 2 * rovnice.a;
			rovnice.complex.imag = (sqrt(-rovnice.disc)) / 2 * rovnice.a;
		}
	}
	else if (rovnice.type == linearni) {
		rovnice.x1 = (0 - rovnice.c) / rovnice.b;
	}
}

void vyres(Rovnice *rovnice, int n) {
	for (int i = 0; i < n; i++) {
		calculate(rovnice[i]);
	}
}

int nactiCisloRovnice(int n) {
	int i;
	cout << "Zadej cislo rovnice, kterou chces vypsat (1-"<< n <<"):" << endl;
	while (true) {
		cout << "i = ";
		cin >> i;
		if (!cin || !(i >= 1 && i <= n)) {
			clearCin();
			cout << "Zadejte pouze cislo od 1 do " << n << endl;
			continue;
		}
		break;
	}
	return i - 1;
}

string oper(double e, bool sign) {
	if (e > 0 && sign) {
		return " + ";
	}
	else if (e < 0) {
		return " - ";
	}
	return " ";
}



void vypisRovnici(Rovnice *rovnice, int i) {
	Rovnice r = rovnice[i];
	cout << "Rovnice c. " << i+1 << ": "
		<< oper(r.a, false) << abs(r.a) << "x^2"
		<< oper(r.b, true) << abs(r.b) << "x"
		<< oper(r.c, true) << abs(r.c) << " = 0" << endl;
}

bool rozhodnuti(string message) {
	char answer;
	bool r = true;
	while (true) {
		cout << message;
		cin >> answer;
		if (!cin || !(answer == 'a' || answer == 'n')) {
			clearCin();
			cout << "Zadejte pouze jedno ze dvou pismen (a/n):" << endl;
			continue;
		}
		else {
			if (answer != 'a')
				r = false;
			break;
		}
	}
	return r;
}

bool vypisReseni(Rovnice rovnice) {
	if (!rozhodnuti("Chces vypsat reseni rovnice? (a/n):")) {
		return false;
	}
	
	if (rovnice.type == kvadraticka) {
		if(rovnice.disc > 0){
			cout << "Jedna se o kvadratickou rovnici. Rovnice ma dva realne koreny x1 = " << rovnice.x1 << ", " << "x2 = " << rovnice.x2 << endl;
		}
		else if (rovnice.disc == 0) {
			cout << "Jedna se o kvadratickou rovnici. Rovnice ma dva totozne realne koreny x1,2 = " << rovnice.x1 << endl;
		}
		else {
			cout << "Jedna se o kvadratickou rovnici. Rovnice ma dva komplexni koreny x1 = "
				<< rovnice.complex.real << " + " << rovnice.complex.imag << "i, " << "x2 = "
				<< rovnice.complex.real << " - " << rovnice.complex.imag << "i" << endl;
		}
	}
	else if (rovnice.type == linearni) {
		cout << "Jedna se o linearni rovnici. Rovnice ma koren x = " << rovnice.x1 << endl;
	}
	else if (rovnice.type == vsechna) {
		cout << "Resenim rovnice jsou vsechna realna cisla" << endl;
	}
	else if (rovnice.type == neresitelna) {
		cout << "Rovnice nema reseni" << endl;
	}
	else {
		cout << "Doslo k chybe, typ rovnice nebyl rozpoznan" << endl;
	}
	return true;
}

void vypis(Rovnice *rovnice, int n) {
	bool running = true;
	while (running) {
		int i = nactiCisloRovnice(n);
		vypisRovnici(rovnice, i);
		running = vypisReseni(rovnice[i]);
		running = rozhodnuti("Chces vypsat dalsi rovnici? (a/n):");
			if (!running) break;
	}
}

int main()
{
	const int n = 10;
	Rovnice rovnice[n];

	nactiZadani(rovnice, n);

	vyres(rovnice, n);
	vypis(rovnice, n);
	return 0;
}
