#include <iostream>
#include <Windows.h>
#include <string>
#include <math.h>
using namespace std;

enum member { a, b, c };

enum rcetype { kvadraticka, linearni, vsechna, neresitelna };

struct Rovnice {
	double a;
	double b;
	double c;
	double disc;
	double x1;
	double x2;
	rcetype typ;
};

void clearCin() {
	cin.clear();
	cin.ignore(256, '\n');
}

void determineType(Rovnice& rovnice) {
	if (rovnice.a == 0) {
		if (rovnice.b == 0) {
			if (rovnice.c == 0) {
				rovnice.typ = vsechna;
			}
			else {
				rovnice.typ = neresitelna;
			}
		}
		else {
			rovnice.typ = linearni;
		}
	}
	else {
		rovnice.typ = kvadraticka;
	}
}

void calculateSolution(Rovnice& rovnice) {
	rovnice.disc = (rovnice.b * rovnice.b - 4 * rovnice.a * rovnice.c);
	//cout << rovnice.b << "^2 - 4*" << rovnice.a << "*" << rovnice.c << " = Diskriminant (" << rovnice.disc << ")\n" << endl;
	determineType(rovnice);
	if (rovnice.typ == kvadraticka) {
		if (rovnice.disc > 0) { //dva realne koreny
			rovnice.x1 = (-rovnice.b + sqrt(rovnice.disc) / 2 * rovnice.a);
			rovnice.x2 = (-rovnice.b - sqrt(rovnice.disc) / 2 * rovnice.a);
			cout << "Jedna se o kvadratickou rovnici. Rovnice ma dva realne koreny x1 = " << rovnice.x1 << ", " << "x2 = " << rovnice.x2 << endl;
		}
		else if (rovnice.disc == 0) { //dva stejne realne koreny
			rovnice.x1 = (-rovnice.b + sqrt(rovnice.disc) / 2 * rovnice.a);
			rovnice.x2 = rovnice.x1;
			cout << "Jedna se o kvadratickou rovnici. Rovnice ma dva totozne realne koreny x1,2 = " << rovnice.x1 << endl;
		}
		else {
			double b, d;
			b = (-rovnice.b) / 2 * rovnice.a;
			d = (sqrt(-rovnice.disc)) / 2 * rovnice.a;
			cout << "Jedna se o kvadratickou rovnici. Rovnice ma dva komplexni koreny x1 = " << b << " + " << d << "i, " << "x2 = " << b << " - " << d << "i" << endl;
		}
	}
	else if (rovnice.typ == linearni) {
		rovnice.x1 = (0 - rovnice.c) / rovnice.b;
		cout << "Jedna se o linearni rovnici. Rovnice ma koren x = " << rovnice.x1 << endl;
	}
	else if (rovnice.typ == vsechna) {
		cout << "Resenim rovnice jsou vsechna realna cisla" << endl;
	}
	else if (rovnice.typ == neresitelna) {
		cout << "Rovnice nema reseni" << endl;
	}
	else {
		cout << "Doslo k chybe, typ rovnice nebyl rozpoznan" << endl;
	}
}

char printSolution(Rovnice& rovnice) {
	char running = 'a';
	while (true) {
		cout << "Chces vypsat reseni rovnice? (a/n):";
		cin >> running;
		if (!cin || !(running == 'a' || running == 'n')) {
			clearCin();
			cout << "Zadejte pouze jedno ze dvou pismen (a/n):" << endl;
			continue;
		}
		if (running == 'a') {
			calculateSolution(rovnice);
			while (true) {
				cout << "Chces vypsat dalsi rovnici? (a/n):";
				cin >> running;
				if (!cin || !(running == 'a' || running == 'n')) {
					clearCin();
					cout << "Zadejte pouze jedno ze dvou pismen (a/n):" << endl;
					continue;
				}
				break;
			}
		}
		break;
	}
	return running;
}

void loadCoefs(Rovnice* rovnice, int n) {
	for (int i = 0; i < n; i++) {
		cout << "Rovnice č. " << i + 1 << endl;
		string rce[3] = { "a = ","b = ","c = " };
		for (int j = 0; j < 3; j++) {
			while (true) {
				cout << rce[j];
				switch (j) {
				case 0:
					cin >> rovnice[i].a;
					break;
				case 1:
					cin >> rovnice[i].b;
					break;
				case 2:
					cin >> rovnice[i].c;
					break;
				}
				if (!cin) {
					clearCin();
					cout << "Cislo neni platne. Zadejte jej znovu" << endl;
					continue;
				}
				break;
			}
		}
	}
}

string op(double e, bool sign) {
	if (e > 0 && sign) {
		return " + ";
	}
	else if (e < 0) {
		return " - ";
	}
	return " ";
}

int printEquation(Rovnice* rovnice) {
	int i;
	cout << "Zadej číslo rovnice, kterou chceš vypsat (1-10):" << endl;
	cout << "i="; cin >> i;
	Rovnice r = rovnice[i - 1];
	cout << "Rovnice č. " << i << ": "
		<< op(r.a, false) << r.a << "x²"
		<< op(r.b, true) << r.b << "x"
		<< op(r.c, true) << r.c << " = 0" << endl;
	return i - 1;
}

int main()
{
	SetConsoleOutputCP(CP_UTF8);
	char running = 'a';
	int n = 10;
	Rovnice rovnice[10];

	cout << "Zadej 10 rovnic ve tvaru ax² + bx + c = 0\n";
	loadCoefs(rovnice, n);

	while (true) {
		int i = printEquation(rovnice);
		char running = printSolution(rovnice[i]); //toto musí do printEquation (jinak nepoznám číslo rovnice, haha - nebo(!) mi tohle cislo vrati printEquation)
		if (running == 'n')
			break;
	}
	return 0;
}
