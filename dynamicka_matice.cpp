#include <iostream>
#include <string>
#include <new>
#include <iomanip>

using namespace std;

float **vytvor(int n, int m) {
	float **a; //matice: ukazatel na ukazatel
	a = new float*[n]; //pole ukazatelů délky n
	for (int i = 0; i < n; i++) {
		a[i] = new float[m];
	}
	//práce s maticí

	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) {
			a[i][j] = (float)((i + 1)*(j + 1));
		}
	}
	return a;
}

void vypis(float **a, int n, int m) {
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) {
			cout << setw(3) << a[i][j] << ' ';
		}
		cout << endl;
	}
}

void zrus(float **a, int n) {
	for (int i = 0; i < n; i++) {
		delete[] a[i];
	}
}

int main()
{
	int n, m;
	cout << "Zadej pocet radku: ";
	cin >> n;
	if (n <= 0)
		n = 1;
	cout << "Zadej pocet sloupcu: ";
	cin >> m;
	if (m <= 0)
		m = 1;

	//alokace
	float **a = vytvor(n, m);
	float **b = vytvor(n, m);

	//float **c = secti(a,b,n,m);
	//float **d = transponuj(n, m);

	//TODO: vytvořit funkce a provést


	//vypis
	vypis(a, n, m);

	//dealokace
	zrus(a, n);


	return 0;
}