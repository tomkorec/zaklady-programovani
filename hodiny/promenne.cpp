#include <iostream>
#include <string>
#include <new>

using namespace std;

void vypis(int i) {
	static string nazvy[4] = { "sever","jih","vychod","zapad" }; //globální proměnná platná jen pro tuto funkci (ale uložena v části paměti pro globální proměnné)
	cout << nazvy[i];
}

struct Zlomek {
	int citatel, jmenovatel;
};

void vypisZlomek(Zlomek &z) {
	cout << z.citatel << " / " << z.jmenovatel << endl;
}

void odstrasujici() {
	int *u;
	while (true)
		u = new int[1000000];
}

void odstrasujici2() {
	int *u;
	try {
		while (true)
			u = new int[1000000];
	}
	catch (const bad_alloc &e) {
		cout << "chyba!! takhle dojde pamet: " << e.what() << endl;
	}
}

void odstrasujici3() {
	int *u;
	while (true)
		u = new (nothrow) int[1000000];
}

int main()
{
	{//DYNAMICKÁ STRUKTURA

		Zlomek z = { 1,2 };
		z.citatel = 3;
		z.jmenovatel = 5;

		Zlomek *uz = new Zlomek;
		uz->jmenovatel = 2; // stejne jako (*uz).jmenovatel = 2;
		uz->citatel = 3;

		vypisZlomek(*uz);
		delete uz;
		//garbage collector (v C# nebo v Javě - automatické mazání: netoliko v C++)

		//odstrasujici(); //paměť se alokuje, ale neuvolňuje
		//odstrasujici2(); //odchycení výjimky: jenomže stejnak máme už hodně vyčerpanou paměť
		odstrasujici3(); //nehodi výjimku, takže pořád poběží
		

	}
	
	{ //DYNAMICKÉ PROMĚNNÉ
		int *ui = new int; //alokace dynamické proměnné
		*ui = 6;
		cout << *ui << endl;
		cout << ui << endl;

		delete ui; //dealokace (vymaže místo v paměti)
			//*ui = 3; CHYBA! - nebezpečí přepsání paměti, která mi nepatří
		ui = nullptr; //vynuluje ukazatel - předchází chybě --^
		
		ui = new int; //opětovná alokace
		cout << ui << endl; //ale už to bude na jiný adrese, SAMOZŘEJMĚ (protože ta původní už může být obsazena)
	}
	
	{//DYNAMICKY ALOKOVANÉ POLE
		int n;
		cout << "zadej pocet prvku pole" << endl;
		cin >> n;
		if (n < 1) {
			n = 1;
		}
		int *ui;
		ui = new int[n];
		for (int i = 0; i < n; i++) {
			ui[i] = i;
		}
		for (int i = 0; i < n; i++) {
			cout << *(ui + i) << ' ';
		}
		cout << endl;

		delete[] ui;


	}

	{//GLOBÁLNÍ PROMĚNNÉ
		int x = 0;
		vypis(x);
	}
}