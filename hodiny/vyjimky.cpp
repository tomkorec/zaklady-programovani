#include <iostream>
#include <string>
#include <new>

using namespace std;

int vydel(int a, int b) {
	if (b == 0)
		throw 1;
	if (a == 0)
		throw (string) "a je 0!!!";
	if (a == 2)
		throw exception("a je 2");
	return a / b;
}

int main()
{
	int a = 2, b = 3;
	try {
		cout << vydel(a, b) << endl;

	}
	catch (int e) {
		cout << "nastala chyba" << endl;
		switch (e) {
			case 1:
				cout << "deleni nulou" << " zadej b znova";
		}
	}
	catch (const string &e) {
		cout << "nastala chyba " << e << endl;
	}
	catch (const exception &e) {
		cout << "chyba: " << e.what() << endl;
	}
	catch (...) {
		cout << "nastala neocekavana chyba" << endl;
	}
	return 0;
}