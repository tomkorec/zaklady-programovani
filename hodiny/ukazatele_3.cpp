#include <iostream>
using namespace std;

void vypis(int *a, int n) {
	int *p = a;
	while (p < a + 10) {
		cout << *p << ' ';
		p++;
	}
	cout << endl;
}

int *maximum(int *a, int n) {
	int *p = a;
	int *maximum = a;
	while (p < a + 10) {

		if (*p > *maximum) {
			maximum = p;
		}
		p++;
	}
	return maximum;
}


int main()
{
	int a[10] = {1,2,3,8,5,12,3,1,2,10};
	vypis(a, 10);
	cout << *maximum(a,10) << endl;
	return 0;
}