#include <iostream>
using namespace std;

struct Zlomek {
	int citatel, jmenovatel;
};


void vypisPole(int *a, int n) {
	for (int i = 0; i < n; i++) {
		cout << a[i] << ' ';
	}
	cout << endl;
}

void vypis(const Zlomek *z) {
	cout << z->citatel << " / " << z->jmenovatel << endl;
}

int *paty(int *a) {
	int *p = &a[4];
	return p;
}

int &paty1(int *a) {
	return a[4];
}

int main()
{
	int a[10] = { 1,2,3,4,5,6,7,8,9,10 };
	int *ua = &a[0]; //ukazuje na prvni prvek
	int *va = &a[2]; //ukazuje na tretiprvek
	int *wa = a; //ukazuje take na prvni prvek
	int x = 4;
	
	vypisPole(a, 10);
	vypisPole(ua, 10);
	vypisPole(wa, 10);
	vypisPole(va,8);
	vypisPole(&x, 1);

	Zlomek z = { 1,2 };
	vypis(&z);

	Zlomek *uz;
	uz = &z;
	vypis(uz);

	uz->citatel = 1; // (*uz).citatel = 1;
	uz->jmenovatel = 7; // (*uz).citatel = 1;
	
	vypis(&z);
	
	int *p = paty(a);
	*p = 0;
	vypisPole(a, 10);
	*paty(a) = 100; //přiřazovací funkce 
	vypisPole(a, 10);

	cout << *paty(a) << endl;

	cout << &paty1(a) << endl;

	paty1(a) = 1000; //přiřazovací funkce
	int *q = &paty1(a);
	cout << *q << endl;
	cout << paty1(a);
	return 0;
}