#include <iostream>
#include <fstream>
#include <string>
using namespace std;

void nactiZeSouboru(string nazev) {
	string s;
	ifstream soubor;
	soubor.open(nazev);
	if (soubor.is_open()) {
		while (!soubor.eof()) {
			getline(soubor, s);
			cout << s << endl;
		}
		soubor.close();
	}
	else {
		cout << "nepodarilo se otevrit soubor";
	}
}

int pocetRadku(string nazev) {
	string s;
	ifstream soubor;
	soubor.open(nazev);
	int rowCount = 0;
	if (soubor.is_open()) {
		while (!soubor.eof()) {
			getline(soubor, s);
			rowCount++;
		}
		soubor.close();
	}
	else {
		cout << "soubor se nepodarilo nacist";
	}
	return rowCount;
}

string *nactiRadky(string nazev, int n) {
	string *pole = new string[n];
	string s;
	ifstream soubor;
	soubor.open(nazev);
	int rowCount = 0;
	if (soubor.is_open()) {
		for (int i = 0; i < n; i++) {
			getline(soubor, s);
			pole[i] = s;
		}
		soubor.close();
	}
	else {
		cout << "soubor se nepodarilo nacist";
	}
	return pole;
}

void vypisRadkyOpacne(string *pole, int n, string nazev) {
	ofstream soubor;
	soubor.open(nazev);
	if (soubor.is_open()) {
		for (int i = n - 1; i >= 0; i--) {
			cout << pole[i] << endl;
			soubor << pole[i] << endl;
		}
		soubor.close();
	}
	else {
		cout << "chyba: soubor s nepodarilo otevrit\n";
	}
}



int main()
{

	cout << "pocet radku: " << pocetRadku("prvni.txt") << endl;
	int rowCount = pocetRadku("prvni.txt");
	string *pole = nactiRadky("prvni.txt", rowCount);
	vypisRadkyOpacne(pole, rowCount, "druhy.txt");
	delete[] pole;
	return 0;
}
