#include <iostream>
using namespace std;

struct ChytrePole {
	float *a = nullptr;
	int n = 0; //počet prvků pole
	int k = 0; //kapacita pole
};

void zrus(ChytrePole &s) {
	if (s.a != nullptr) {
		delete[] s.a;
		s.a = nullptr;
		s.n = 0;
		s.k = 0;
	}
}

void pridej(ChytrePole &s, float x) {
	if (s.a == nullptr) {
		cout << "Zatim tu nic neni" << endl;
		s.k = 1;
		s.a = new float[s.k];
	}
	if (s.k == s.n) {
		cout << "Kapacita pole " << s.k << " vycerpana. Zvysena na " << s.k*2 << endl;
		float *a = new float[2*s.k];
		for (int i = 0; i < s.n; i++) {
			a[i] = s.a[i];
		}
		delete[] s.a;
		s.k *= 2;
		s.a = a;
	}
	s.a[s.n] = x;
	s.n += 1;
}

void vypis(ChytrePole &s) {
	for (int i = 0; i < s.n; i++) {
		cout << s.a[i] << " ";
	} cout << endl;
}

void smaz(ChytrePole &s, int i) {
	if (i >= 0 && i < s.n) {
		for (int j = i+1; j < s.n; j++) {
			s.a[j-1] = s.a[j];
		}
		s.n -= 1;
	}
}

int najdi(ChytrePole &s, float x) {
	int i = -1;
	//
	for (int j = 0; j < s.n; j++) {
		if (x == s.a[j])
			i = j;
	}
	if (i == -1)
		throw exception("Nepovedlo se nalezt dany prvek");
	return i;
}

int main()
{
	ChytrePole s;
	int i = 0;

	while (true)
	{
		cout << "zadej dalsi cislo (zadavani ukoncis zadanim jineho znaku): ";
		float x;
		cin >> x;
		if (cin.fail())
			break;
		pridej(s, x);
		i++;
	}
	vypis(s);
	smaz(s, 1);
	vypis(s);

	try {
		cout << "Osmicka se nachazi na indexu " << najdi(s, 8);
	}
	catch (const exception &e) {
		cout << "chyba: " << e.what() << endl;
	}

	
	//zrus(s);
	return 0;
}
