#include <iostream>
using namespace std;

void prohod(int *pa, int *pb) {
	int c = *pa;
	*pa = *pb;
	*pb = c;
}

void neprohod1(int *pa, int *pb) {
	int *pc = pa;
	pa = pb;
	pb = pc;
}

int main()
{
	{
		double x = 3.25;
		cout << "adresa x je " << &x << " a hodnota x je " << x << endl;
		double *px;
		px = &x;

		*px = 4.67;
		*px = (*px) * (-1);
		cout << "nova hodnota x je " << x << endl;
	}
	{
		int a =1 , b =2;
		prohod(&a, &b);
		//neprohod1(&a,&b);
		cout << a << " a " << b << endl;
	}
	{

	}

return 0;
}
