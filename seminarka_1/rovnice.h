#pragma once
#include <string>
using namespace std;

enum typ { kvadraticka, linearni, vsechna, neresitelna };

struct Complex {
	double real;
	double imag;
};

struct Rovnice {
	double a;
	double b;
	double c;
	double x1;
	double x2;
	Complex complex;
	double disc;
	typ type;
};

void nactiZadani(Rovnice *rovnice, int n);
void determineType(Rovnice &rovnice);
void calculate(Rovnice &rovnice);
void vyres(Rovnice  *rovnice, int n);
int nactiCisloRovnice(int n);
string oper(double e, bool sign);
void vypisRovnici(Rovnice* rovnice, int i);
bool rozhodnuti(string message);
bool vypisReseni(Rovnice rovnice);
void vypis(Rovnice* rovnice, int n);