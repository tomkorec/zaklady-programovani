#include <iostream>
#include <string>
#include <math.h>
#include "Rovnice.h"
using namespace std;
int main()
{
	const int n = 10;
	Rovnice rovnice[n];

	nactiZadani(rovnice, n);
	vyres(rovnice, n);
	vypis(rovnice, n);
	return 0;
}
